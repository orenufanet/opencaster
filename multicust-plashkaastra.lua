#!/usr/bin/astra

make_channel({
    name = "name_avaria",
    input = {
        "file:///home/administrator/opencaster/multicust-plashka/output/avaria_name.ts#loop",
    },
    output = {
        "udp://eth0.99@239.0.0.2:1234",
    },

})

make_channel({
    name = "name_interferencia",
    input = {
        "file:///home/administrator/opencaster/multicust-plashka/output/interferencia_name.ts#loop",
    },
    output = {
        "udp://eth0.99@239.0.0.2:1234",
    },

})

make_channel({
    name = "name_delete",
    input = {
        "file:///home/administrator/opencaster/multicust-plashka/output/delete_name.ts#loop",
    },
    output = {
        "udp://eth0.99@239.0.0.2:1234",
    },

})

make_channel({
    name = "name_moved",
    input = {
        "file:///home/administrator/opencaster/multicust-plashka/output/moved_name.ts#loop",
    },
    output = {
        "udp://eth0.99@239.0.0.2:1234",
    },

})

make_channel({
    name = "name_moved_f",
    input = {
        "file:///home/administrator/opencaster/multicust-plashka/output/moved_f_name.ts#loop",
    },
    output = {
        "udp://eth0.99@239.0.0.2:1234",
    },

})

make_channel({
    name = "name_profilactika",
    input = {
        "file:///home/administrator/opencaster/multicust-plashka/output/profilactikaname.ts#loop",
    },
    output = {
        "udp://eth0.99@239.0.0.2:1234",
    },

})

