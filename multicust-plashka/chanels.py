# -*- coding: utf-8 -*-
streams = [
    {'name': '282_290',
     'avaria': '239.0.0.1:1234',
     'interferencia': '239.0.0.2:1234',
     'avaria_mp4': '239.0.0.3:1234',
     # 'profilactika': '239.0.0.4:1234',
     # 'avaria_mp4_hd': '239.0.0.5:1234',
     # 'profilactika_mp4': '239.0.0.6:1234',
     'chanels': [
         {
             'name': 'Fightbox',
             'sid': 212,
         },
         {
             'name': 'ZHARA',
             'sid': 2055,
         },
         {
             'name': 'Pervyj kanal HD',
             'sid': 2001,
         },
         {
             'name': 'Kinohit',
             'sid': 2052,
         },
         {
             'name': 'HD Life',
             'sid': 2053,
         },
         {
             'name': 'MCM TOP',
             'sid': 2054,
         },       
         {
             'name': 'Mama',
             'sid': 4160,
         },
         {
             'name': 'TNT4 +2',
             'sid': 2057,
         },
         {
             'name': 'Kinomiks',
             'sid': 2060,
         },
         {
             'name': 'Nauka',
             'sid': 2063,
         },
         {
             'name': 'Nickelodeon HD',
             'sid': 2576,
         },
         {
             'name': 'Nick Jr',
             'sid': 2577,
         },
         {
             'name': 'Match! Futbol 1 HD',
             'sid': 2578,
         }
     ]},

    {'name': '298_306',
     'avaria': '239.0.0.7:1234',
     'interferencia': '239.0.0.8:1234',
     # 'avaria_mp4': '239.0.0.9:1234',
     # 'profilactika': '239.0.0.10:1234',
     # 'avaria_mp4_hd': '239.0.0.11:1234',
     # 'profilactika_mp4': '239.0.0.12:1234',
     'chanels': [
         {
             'name': 'Telekanal Multimuzyka',
             'sid': 2070,
         },
         {
             'name': 'Teatr',
             'sid': 2067,
         },
         {
             'name': 'MTV Rocks',
             'sid': 2064,
         },
         {
             'name': 'English Club TV',
             'sid': 2065,
         },
         {
             'name': 'ZHivaya priroda HD',
             'sid': 2068,
         },
         {
             'name': 'RTG HD',
             'sid': 2069,
         },
         {
             'name': 'TV-3',
             'sid': 2050,
         },
         {
             'name': 'T24',
             'sid': 2593,
         },
         {
             'name': 'Sarafan',
             'sid': 2613,
         },
         {
             'name': 'Motorsport TV',
             'sid': 2402,
         },
         {
             'name': 'Moya planeta',
             'sid': 4296,
         },
         {
             'name': 'Match TV HD',
             'sid': 2074,
         },
         {
             'name': 'Zoo TV',
             'sid': 2077,
         },
         {
             'name': 'Kinopokaz',
             'sid': 2078,
         },
         {
             'name': 'Zdorove',
             'sid': 2079,
         },
         {
             'name': 'Radio ENERGY',
             'sid': 2610,
         },
         {
             'name': 'Avtoradio',
             'sid': 2611,
         },
         {
             'name': 'YUmor FM',
             'sid': 2612,
         },
         {
             'name': 'MUZ',
             'sid': 2100,
         },

     ]},

    {'name': '314_322',
     'avaria': '239.0.0.13:1234',
     'interferencia': '239.0.0.14:1234',
     # 'avaria_mp4': '239.0.0.15:1234',
     # 'profilactika': '239.0.0.16:1234',
     # 'avaria_mp4_hd': '239.0.0.17:1234',
     # 'profilactika_mp4': '239.0.0.18:1234',
     'chanels': [
	     {
             'name': 'Cinema',
             'sid': 2123,
         },
         {
             'name': 'V gostyah u skazki',
             'sid': 2087,
         },
         {
             'name': 'Zee Russia',
             'sid': 2085,
         },
         {
             'name': 'Uspekh',
             'sid': 2084,
         },
         {
             'name': 'Match! Arena HD',
             'sid': 2086,
         },
         {
             'name': 'Peterburg 5 kanal',
             'sid': 1050,
         },
         {
             'name': 'Rossiya K',
             'sid': 1060,
         },
         {
             'name': 'Mayak',
             'sid': 1120,
         },
         {
             'name': 'Vesti FM',
             'sid': 1110,
         },
         {
             'name': 'Radio Rossii',
             'sid': 1130,
         },
         {
             'name': 'ACB TV',
             'sid': 2630,
         },

         {
             'name': 'Nostalgiya',
             'sid': 2645,
         },
         {
             'name': 'KINOKOMEDIYA',
             'sid': 2646,
         },
         {
             'name': 'Dom Kino',
             'sid': 2088,
         },
         {
             'name': 'Match! Nash Sport',
             'sid': 2089,
         },
         {
             'name': 'Muzyka Pervogo',
             'sid': 2090,
         },
         {
             'name': 'Nashe novoe kino',
             'sid': 2092,
         },
         {
             'name': 'Nickelodeon',
             'sid': 2093,
         },
         {
             'name': 'Vremya',
             'sid': 2094,
         },
         {
             'name': 'Match TV',
             'sid': 1030,
         },
         {
             'name': 'Karusel',
             'sid': 1080,
         },
         {
             'name': 'Zvezda',
             'sid': 2070,
         },
         {
             'name': 'STS Love +2',
             'sid': 2643,
         },
         {
             'name': 'Vmeste RF',
             'sid': 2644,
         },
         {
             'name': 'KINOKOMEDIYA',
             'sid': 2646,
         }

     ]},

    {'name': '330_338',
     'avaria': '239.0.0.19:1234',
     'interferencia': '239.0.0.20:1234',
     # 'avaria_mp4': '239.0.0.21:1234',
     # 'profilactika': '239.0.0.22:1234',
     # 'avaria_mp4_hd': '239.0.0.23:1234',
     # 'profilactika_mp4': '239.0.0.24:1234',
     'chanels': [
         {
             'name': 'Russkij detektiv',
             'sid': 2091,
         },
		 {
             'name': 'Illusion+',
             'sid': 2175,
         },
         {
             'name': 'Telekanal Doktor',
             'sid': 2108,
         },
         {
             'name': 'Drajv',
             'sid': 2096,
         },
         {
             'name': 'Soyuz',
             'sid': 2097,
         },
         {
             'name': 'Usadba',
             'sid': 2098,
         },
         {
             'name': 'Retro',
             'sid': 2099,
         },
         {
             'name': 'Zdorovoe TV',
             'sid': 2100,
         },
         {
             'name': 'Ohota i Rybalka',
             'sid': 2101,
         },
         {
             'name': 'Bober',
             'sid': 2102,
         },
         {
             'name': 'Kanal Disney +2',
             'sid': 2656,
         },
         {
             'name': 'Illyuzion + HD',
             'sid': 2657,
         },
         {
             'name': 'Zoopark',
             'sid': 2658,
         },
         {
             'name': 'Detskij HD',
             'sid': 2104,
         },
         {
             'name': 'RT HD',
             'sid': 2105,
         },
         {
             'name': 'OTR',
             'sid': 1090,
         },
         {
             'name': 'CHe +2',
             'sid': 2111,
         },
         {
             'name': 'Ohotnik i rybolov HD',
             'sid': 2672,
         },
         {
             'name': '8 kanal',
             'sid': 2107,
         }

     ]},

    {'name': '346_354',
     'avaria': '239.0.0.25:1234',
     'interferencia': '239.0.0.26:1234',
     # 'avaria_mp4': '239.0.0.27:1234',
     # 'profilactika': '239.0.0.28:1234',
     # 'avaria_mp4_hd': '239.0.0.29:1234',
     # 'profilactika_mp4': '239.0.0.30:1234',
     'chanels': [
         {
             'name': 'Eda Premium',
             'sid': 2110,
         },
         {
             'name': 'Galaxy',
             'sid': 4,
         },
         {
             'name': 'Ren-TV',
             'sid': 2010,
         },
         {
             'name': 'Match! Boec',
             'sid': 2115,
         },
         {
             'name': 'Kuhnya TV',
             'sid': 2117,
         },
         {
             'name': 'Domashnij',
             'sid': 2040,
         },
         {
             'name': 'Kinosemya',
             'sid': 2119,
         },
         {
             'name': 'Russkij bestseller',
             'sid': 2116,
         },
         {
             'name': 'TMT Majdan',
             'sid': 2121,
         },
         {
             'name': 'A2',
             'sid': 2122,
         },
         {
             'name': 'NST',
             'sid': 2126,
         },
         {
             'name': 'BST Oren Orsk LCN',
             'sid': 2704,
         },
         {
             'name': 'Shopping Live',
             'sid': 2706,
         },
         {
             'name': 'YUldash',
             'sid': 2707,
         },
         {
             'name': 'Sputnik FM',
             'sid': 2708,
         },
         {
             'name': 'Dozhd HD',
             'sid': 2125,
         }

     ]},

    {'name': '362_370',
     'avaria': '239.0.0.31:1234',
     'interferencia': '239.0.0.32:1234',
     # 'avaria_mp4': '239.0.0.33:1234',
     # 'profilactika': '239.0.0.34:1234',
     # 'avaria_mp4_hd': '239.0.0.35:1234',
     # 'profilactika_mp4': '239.0.0.36:1234',
     'chanels': [
         {
             'name': 'POEKHALI!',
             'sid': 2135,
         },
         {
             'name': 'Spas',
             'sid': 2020,
         },
         {
             'name': 'Morskoj',
             'sid': 2131,
         },
         {
             'name': 'Match! Futbol 2 HD',
             'sid': 2129,
         },
         {
             'name': 'Mult',
             'sid': 4738,
         },
         {
             'name': 'Rodnoe kino',
             'sid': 4745,
         },
         {
             'name': 'Kto est Kto',
             'sid': 2132,
         },
         {
             'name': 'DW (Deutsche Welle)',
             'sid': 2133,
         },
         {
             'name': 'Tiji',
             'sid': 2134,
         },
         {
             'name': 'Topshop TV',
             'sid': 2130,
         },
         {
             'name': 'O!',
             'sid': 2149,
         },
         {
             'name': 'Malysh',
             'sid': 2150,
         },
         {
             'name': 'Eda',
             'sid': 2144,
         },
         {
             'name': 'TNT',
             'sid': 2090,
         },
         {
             'name': 'Oruzhie',
             'sid': 2146,
         },
         {
             'name': 'TELEKAFE',
             'sid': 2148,
         },
         {
             'name': 'NTV',
             'sid': 1040,
         },
         {
             'name': 'TVC',
             'sid': 1100,
         },
         {
             'name': 'Ginger HD',
             'sid': 2147,
         }

     ]},

    {'name': '378_386',
     'avaria': '239.0.0.37:1234',
     'interferencia': '239.0.0.38:1234',
     # 'avaria_mp4': '239.0.0.39:1234',
     # 'profilactika': '239.0.0.40:1234',
     # 'avaria_mp4_hd': '239.0.0.41:1234',
     # 'profilactika_mp4': '239.0.0.42:1234',
     'chanels': [
         {
             'name': 'Super +2',
             'sid': 2401,
         },
         {
             'name': 'RUSSIAN MUSICBOX',
             'sid': 2152,
         },
         {
             'name': 'MUSICBOX',
             'sid': 2153,
         },
         {
             'name': 'Russkaya noch',
             'sid': 2154,
         },
         {
             'name': 'Indijskoe kino',
             'sid': 2156,
         },
         {
             'name': 'Lya minor',
             'sid': 2157,
         },
         {
             'name': 'Avto plyus',
             'sid': 2158,
         },
         {
             'name': '365 dnej TV',
             'sid': 4928,
         },
         {
             'name': 'KINO TV',
             'sid': 2769,
         },
         {
             'name': 'Radio Mir',
             'sid': 2771,
         },
         {
             'name': 'Match! Igra HD',
             'sid': 2155,
         },
         {
             'name': 'Dorama',
             'sid': 2160,
         },
         {
             'name': 'Futbol',
             'sid': 2229,
         },
         {
             'name': 'SHanson TV',
             'sid': 2161,
         },
         {
             'name': 'Russkij roman',
             'sid': 2162,
         },
         {
             'name': 'KKHL HD',
             'sid': 2165,
         },
         {
             'name': 'Fashion One HD',
             'sid': 2166,
         },
         {
             'name': 'Rossiya 24',
             'sid': 1070,
         }

     ]},

    {'name': '394_402',
     'avaria': '239.0.0.43:1234',
     'interferencia': '239.0.0.44:1234',
     # 'avaria_mp4': '239.0.0.45:1234',
     # 'profilactika': '239.0.0.46:1234',
     # 'avaria_mp4_hd': '239.0.0.47:1234',
     # 'profilactika_mp4': '239.0.0.48:1234',
     'chanels': [
         {
             'name': 'MTV Live HD',
             'sid': 2169,
         },
         {
             'name': 'Russkij Illyuzion HD',
             'sid': 2170,
         },
         {
             'name': 'Russkij ehkstrim',
             'sid': 2171,
         },
         {
             'name': 'ZHivaya planeta',
             'sid': 2173,
         },
         {
             'name': 'Evrokino HD',
             'sid': 5057,
         },
         {
             'name': 'Mezzo',
             'sid': 5058,
         },
         {
             'name': 'TatMuzTV',
             'sid': 2172,
         },
         {
             'name': 'КИНОПРЕМЬЕРА (HD)',
             'sid': 2185,
         },
         {
             'name': 'Киносвидание',
             'sid': 5192,
         },
         {
             'name': 'Рыжий',
             'sid': 2186,
         },
         {
             'name': 'КИНОСЕРИЯ',
             'sid': 2187,
         },
         {
             'name': 'VH 1 European',
             'sid': 2818,
         },
         {
             'name': 'Мир 24',
             'sid': 2190,
         },
         {
             'name': 'BRIDGE TV РУССКИЙ ХИТ',
             'sid': 2191,
         }

     ]},

    {'name': '410_418',
     'avaria': '239.0.0.49:1234',
     'interferencia': '239.0.0.50:1234',
     # 'avaria_mp4': '239.0.0.51:1234',
     # 'profilactika': '239.0.0.52:1234',
     # 'avaria_mp4_hd': '239.0.0.53:1234',
     # 'profilactika_mp4': '239.0.0.54:1234',
     'chanels': [
         {
             'name': 'E',
             'sid': 2406,
         },
         {
             'name': 'YU',
             'sid': 2136,
         },
         {
             'name': '2X2 +2 Ufa',
             'sid': 4800,
         },
         {
             'name': 'TNT MUSIC',
             'sid': 2137,
         },
         {
             'name': 'PYATNICA',
             'sid': 2060,
         },
         {
             'name': 'RU.TV Ufa',
             'sid': 2139,
         },
         {
             'name': 'MTV Dance',
             'sid': 2140,
         },
         {
             'name': 'RBK-TV',
             'sid': 2141,
         },
         {
             'name': 'TV-XXI',
             'sid': 2142,
         },
         {
             'name': 'TNV',
             'sid': 2143,
         },
         {
             'name': 'Zagorodnyj',
             'sid': 2736,
         },
         {
             'name': 'Match! Premer HD',
             'sid': 2195,
         },
         {
             'name': 'Brazzers TV Europe',
             'sid': 2193,
         },
         {
             'name': 'Travel + Adventure HD',
             'sid': 2194,
         },

     ]},

    {'name': '426_434',
     'avaria': '239.0.0.55:1234',
     'interferencia': '239.0.0.56:1234',
     # 'avaria_mp4': '239.0.0.57:1234',
     # 'profilactika': '239.0.0.58:1234',
     # 'avaria_mp4_hd': '239.0.0.59:1234',
     # 'profilactika_mp4': '239.0.0.60:1234',
     'chanels': [
         {
             'name': 'Tugan Tel',
             'sid': 2176,
         },
         {
             'name': 'Kuraj-TV',
             'sid': 2177,
         },
         {
             'name': 'KKHL TV',
             'sid': 2178,
         },
         {
             'name': 'Pervyj kanal',
             'sid': 1010,
         },
         {
             'name': 'Rossiya 1',
             'sid': 1020,
         },
         {
             'name': 'STS',
             'sid': 2030,
         },
         {
             'name': 'Evronovosti',
             'sid': 2817,
         },
         {
             'name': 'MULTIMANIYA.TV',
             'sid': 2403,
         },
         {
             'name': 'TNV-Planeta',
             'sid': 2213,
         },
         {
             'name': 'Priklyucheniya HD',
             'sid': 2208,
         },
         {
             'name': 'Rossiya HD',
             'sid': 2209,
         },
         {
             'name': 'O-lya-lya',
             'sid': 2210,
         },
         {
             'name': 'Mir',
             'sid': 2080,
         },
         {
             'name': 'Frenchlover',
             'sid': 2212,
         }

     ]},

    {'name': '442_450',
     'avaria': '239.0.0.61:1234',
     'interferencia': '239.0.0.62:1234',
     # 'avaria_mp4': '239.0.0.63:1234',
     # 'profilactika': '239.0.0.64:1234',
     # 'avaria_mp4_hd': '239.0.0.65:1234',
     # 'profilactika_mp4': '239.0.0.66:1234',
     'chanels': [
         {
             'name': 'France 24 (HD)',
             'sid': 2203,
         },
         {
             'name': 'Глазами туриста HD',
             'sid': 2202,
         },
         {
             'name': 'Мужское кино',
             'sid': 2204,
         },
         {
             'name': 'ТРО',
             'sid': 2206,
         },
         {
             'name': 'Психология21',
             'sid': 2205,
         },
         {
             'name': 'Paramount Comedy',
             'sid': 2848,
         },
         {
             'name': 'ORT-Planeta',
             'sid': 21,
         },
         {
             'name': 'AMEDIA HIT HD',
             'sid': 2124,
         },
         {
             'name': 'Travel Channel HD',
             'sid': 2224,
         },
         {
             'name': 'Mezzo Live HD',
             'sid': 2225,
         },
         {
             'name': 'Istoriya',
             'sid': 2226,
         },
         {
             'name': 'Europa Plus TV',
             'sid': 2228,
         },
         {
             'name': 'Playboy TV',
             'sid': 2230,
         },
         {
             'name': 'UTV HD',
             'sid': 2912,
         },
         {
             'name': 'Docubox',
             'sid': 2231,
         }

     ]},

    {'name': '458_466',
     'avaria': '239.0.0.67:1234',
     'interferencia': '239.0.0.68:1234',
     # 'avaria_mp4': '239.0.0.69:1234',
     # 'profilactika': '239.0.0.70:1234',
     # 'avaria_mp4_hd': '239.0.0.71:1234',
     # 'profilactika_mp4': '239.0.0.72:1234',
     'chanels': [
	     {
             'name': 'FAN HD',
             'sid': 2196,
         },
         {
             'name': 'MTV Hits',
             'sid': 2232,
         },
         {
             'name': 'VH 1 Classic',
             'sid': 2233,
         },
         {
             'name': 'A1 HD',
             'sid': 2234,
         },
         {
             'name': 'Avto 24 HD',
             'sid': 2235,
         },
         {
             'name': 'ZHIVI!',
             'sid': 2237,
         },
         {
             'name': 'Ani',
             'sid': 2238,
         },
         {
             'name': 'RTD HD',
             'sid': 2239,
         },
         {
             'name': 'BOKS TV',
             'sid': 2579,
         },
         {
             'name': 'HOME 4K',
             'sid': 2219,
         }

     ]},

    {'name': '610_626',
     'avaria': '239.0.0.73:1234',
     'interferencia': '239.0.0.74:1234',
     # 'avaria_mp4': '239.0.0.75:1234',
     # 'profilactika': '239.0.0.76:1234',
     # 'avaria_mp4_hd': '239.0.0.77:1234',
     # 'profilactika_mp4': '239.0.0.78:1234',
     'chanels': [
         {
             'name': 'V mire zhivotnyh HD',
             'sid': 2240,
         },
         {
             'name': 'EHvrika HD',
             'sid': 2241,
         },
         {
             'name': 'Planeta HD',
             'sid': 2242,
         },
         {
             'name': 'Mir uvlechenij',
             'sid': 2243,
         },
         {
             'name': 'Dom Kino PREMIUM HD',
             'sid': 2253,
         },
         {
             'name': 'Tlum HD',
             'sid': 2062,
         },
         {
             'name': 'World Fashion Channel',
             'sid': 2249,
         },
         {
             'name': 'Match! Futbol 3 HD',
             'sid': 2250,
         },
         {
             'name': 'KVN TV',
             'sid': 2252,
         }

     ]},

    {'name': '474_482',
     'avaria': '239.0.0.79:1234',
     'interferencia': '239.0.0.80:1234',
     # 'avaria_mp4': '239.0.0.81:1234',
     # 'profilactika': '239.0.0.82:1234',
     # 'avaria_mp4_hd': '239.0.0.83:1234',
     # 'profilactika_mp4': '239.0.0.84:1234',
     'chanels': [
         {
             'name': 'TNT MUSIC',
             'sid': 2137,
         },
         {
             'name': 'TNV',
             'sid': 2143,
         },
         {
             'name': 'Eda',
             'sid': 2144,
         },
         {
             'name': 'ZHivaya planeta',
             'sid': 2173,
         },
         {
             'name': 'BST Oren Orsk LCN',
             'sid': 2704,
         },
         {
             'name': 'KINO TV',
             'sid': 2769,
         },
         {
             'name': 'match nash sport',
             'sid': 2089,
         },
         {
             'name': 'KINOKOMEDIYA',
             'sid': 2646,
         },
         {
             'name': 'Illyuzion +',
             'sid': 2175,
         },
         {
             'name': 'Nickelodeon',
             'sid': 2093,
         },
         {
             'name': 'YU',
             'sid': 2136,
         },
         {
             'name': 'Travel+Adventure',
             'sid': 3002,
         },
         {
             'name': 'Telekanal Doktor',
             'sid': 2108,
         },
         {
             'name': 'Tiji',
             'sid': 2134,
         },
         {
             'name': 'T24',
             'sid': 2593,
         },
         {
             'name': 'Shopping Live',
             'sid': 2706,
         },
         {
             'name': 'Dom Kino',
             'sid': 2088,
         },
         {
             'name': 'INDIJSKOE KINO',
             'sid': 2156,
         },
         {
             'name': 'KKHL TV',
             'sid': 2178,
         }

     ]},

    {'name': '490_498',
     'avaria': '239.0.0.85:1234',
     'interferencia': '239.0.0.86:1234',
     # 'avaria_mp4': '239.0.0.87:1234',
     # 'profilactika': '239.0.0.88:1234',
     # 'avaria_mp4_hd': '239.0.0.89:1234',
     # 'profilactika_mp4': '239.0.0.90:1234',
     'chanels': [
         {
             'name': 'Usadba',
             'sid': 2098,
         },
         {
             'name': 'Spas',
             'sid': 2128,
         },
         {
             'name': 'Kanal Disney',
             'sid': 2656,
         },
         {
             'name': 'STS Love',
             'sid': 2643,
         },
         {
             'name': 'ORT-Planeta',
             'sid': 21,
         },
         {
             'name': 'Russkij Illyuzion',
             'sid': 2170,
         },
         {
             'name': 'Moya Planeta',
             'sid': 4296,
         },
         {
             'name': 'RU.TV',
             'sid': 2139,
         },
         {
             'name': 'Russkij roman',
             'sid': 2162,
         },
         {
             'name': 'Istoriya',
             'sid': 2226,
         },
         {
             'name': 'Match! Arena',
             'sid': 2086,
         },
         {
             'name': 'OTR',
             'sid': 2106,
         },
         {
             'name': 'KVN TV',
             'sid': 2252,
         },
         {
             'name': 'TELEKAFE',
             'sid': 2148,
         },
         {
             'name': 'Gorodskoj telekanal UTV',
             'sid': 2182,
         },
         {
             'name': '8 kanal',
             'sid': 2107,
         },
         {
             'name': 'Match! Boec',
             'sid': 2115,
         },
         {
             'name': 'Zoo TV',
             'sid': 2077,
         },
         {
             'name': 'Zdorove',
             'sid': 2079,
         },
         {
             'name': 'ZHivaya priroda HD',
             'sid': 2068,
         },
         {
             'name': 'Mult',
             'sid': 4738,
         },
         {
             'name': 'MUZ',
             'sid': 2608,
         }

     ]},

    {'name': 'roton',
     'avaria': '239.0.0.91:1234',
     'interferencia': '239.0.0.92:1234',
     # 'avaria_mp4': '239.0.0.93:1234',
     # 'profilactika': '239.0.0.94:1234',
     # 'avaria_mp4_hd': '239.0.0.95:1234',
     # 'profilactika_mp4': '239.0.0.96:1234',
     'chanels': [
         {
             'name': 'Pervyj kanal',
             'sid': 1010,
         },
         {
             'name': 'Rossiya 1',
             'sid': 1020,
         },
         {
             'name': 'Match TV',
             'sid': 1030,
         },
         {
             'name': 'NTV',
             'sid': 1040,
         },
         {
             'name': 'Peterburg 5 kanal',
             'sid': 1050,
         },
         {
             'name': 'Rossiya K',
             'sid': 1060,
         },
         {
             'name': 'Rossiya 24',
             'sid': 1070,
         },
         {
             'name': 'Karusel',
             'sid': 1080,
         },
         {
             'name': 'OTR',
             'sid': 1090,
         },
         {
             'name': 'TVC',
             'sid': 1100,
         },
         {
             'name': 'Ren-TV',
             'sid': 2010,
         },
         {
             'name': 'Spas',
             'sid': 2020,
         },
         {
             'name': 'STS',
             'sid': 2030,
         },
         {
             'name': 'Domashnij',
             'sid': 2040,
         },
         {
             'name': 'TV-3',
             'sid': 2050,
         },
         {
             'name': 'PYATNICA',
             'sid': 2060,
         },
         {
             'name': 'Zvezda',
             'sid': 2070,
         },
         {
             'name': 'Mir',
             'sid': 2080,
         },
         {
             'name': 'TNT',
             'sid': 2090,
         },
         {
             'name': 'MUZ',
             'sid': 2100,
         },
         {
             'name': 'ORT-Planeta',
             'sid': 21,
         },
         {
             'name': 'VestiFM',
             'sid': 1110,
         },
         {
             'name': 'Mayak',
             'sid': 1120,
         },
         {
             'name': 'Radio Rossii',
             'sid': 1130,
         }

     ]},

    {'name': 'mono_sid',
     'avaria': '239.0.0.97:1234',
     'interferencia': '239.0.0.98:1234',
     # 'avaria_mp4': '239.0.0.99:1234',
     # 'profilactika': '239.0.0.100:1234',
     # 'avaria_mp4_hd': '239.0.0.101:1234',
     # 'profilactika_mp4': '239.0.0.102:1234',
     'chanels': [
         {
             'name': 'mono_sid',
             'sid': 2404,
         },
     ]},
]
