#!/bin/sh

/etc/init.d/plashki stop



export INPUT1="input/no_signal_reworked_mpeg2video_sd.ts"
export INPUT2="input/interferencia.mpg"
export INPUT3="input/no_signal_reworked_mpeg4video_sd.ts"
export INPUT4="input/planned_work_reworked_mpeg2video_sd.ts"
export INPUT5="input/no_signal_reworked_mpeg4video_hd.ts"
export INPUT6="input/planned_work_reworked_mpeg4video_sd.ts"

mkdir output
mkdir tmp
rm tmp/*
systemctl stop stream-plashki

ffmpeg -i $INPUT1 -an -vcodec mpeg2video -f mpeg2video -b:v 2800k -maxrate 2800k -minrate 2800k -muxrate 3000k -bf 2 -bufsize 3000k -y tmp/video1.mp2
esvideompeg2pes tmp/video1.mp2 > tmp/video1.pes
pesvideo2ts 2064 25 3000000 3000000 0 tmp/video1.pes > tmp/video1.ts

#ffmpeg -i $INPUT1 -ac 2 -vn -acodec mp2 -f mp2 -ab 128000 -ar 48000 -y tmp/audio1.mp2
#esaudio2pes tmp/audio1.mp2 1152 48000 768 -1 3600 > tmp/audio1.pes
#pesaudio2ts 2065 1152 48000 768 -1 tmp/audio1.pes > tmp/audio1.ts



ffmpeg -i $INPUT2 -an -vcodec mpeg2video -f mpeg2video -b:v 2800k -maxrate 2800k -minrate 2800k -muxrate 3000k -bf 2 -bufsize 3000k -y tmp/video2.mp2
esvideompeg2pes tmp/video2.mp2 > tmp/video2.pes
pesvideo2ts 2064 25 3000000 3000000 0 tmp/video2.pes > tmp/video2.ts


ffmpeg -i $INPUT3 -an -vcodec libx264   -f mpegts  -b:v 2800k -maxrate 2800k -minrate 2800k -muxrate 3000k -bf 2 -bufsize 3000k -y tmp/video3.ts
ts2pes tmp/video3.ts 256 > tmp/video3.pes
pesvideo2ts 2064 25 3000000 3000000 0 tmp/video3.pes > tmp/video3.ts

ffmpeg -i $INPUT4 -an -vcodec mpeg2video -f mpeg2video -b:v 2800k -maxrate 2800k -minrate 2800k -muxrate 3000k -bf 2 -bufsize 3000k -y tmp/video4.mp2
esvideompeg2pes tmp/video4.mp2 > tmp/video4.pes
pesvideo2ts 2064 25 3000000 3000000 0 tmp/video4.pes > tmp/video4.ts

ffmpeg -i $INPUT5 -an -vcodec libx264 -f mpegts -b:v 2800k -maxrate 2800k -minrate 2800k -muxrate 3000k -bf 2 -bufsize 3000k -y tmp/video5.ts
ts2pes tmp/video5.ts 256 > tmp/video5.pes
pesvideo2ts 2064 25 3000000 3000000 0 tmp/video5.pes > tmp/video5.ts


ffmpeg -i $INPUT6 -an -vcodec libx264 -f mpegts -b:v 2800k -maxrate 2800k -minrate 2800k -muxrate 3000k -bf 2 -bufsize 3000k -y tmp/video6.ts
ts2pes tmp/video6.ts 256 > tmp/video6.pes
pesvideo2ts 2064 25 3000000 3000000 0 tmp/video6.pes > tmp/video6.ts



python ./create-metadata-ts.py



systemctl start stream-plashki



