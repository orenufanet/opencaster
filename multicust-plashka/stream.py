# coding=utf-8
import os
import threading
import time
import signal
import sys
from chanels import streams
bitrate = 2000000


def ossystem(comand):
    os.system(str(comand))


def stream(file, ip, port):
    name = str(file).split('.')[0]

    os.system('rm ' + str(name) + '_firstfifo.ts')
    os.system('rm ' + str(name) + '_secondfifo.ts')

    # создаем первый fifo файл для общения компонентов opencaster нужен для передачи зацикленного файла
    os.system('mkfifo ' + str(name) + '_firstfifo.ts')

    # создаем первый fifo файл для общения компонентов opencaster нужен для очистки файла от ошибок
    os.system('mkfifo ' + str(name) + '_secondfifo.ts')

    # зацикливаем ts файл
    cmd1 = 'tsloop output/' + str(file) + ' > ' + str(name) + '_firstfifo.ts'
    thread1 = threading.Thread(target=ossystem, args=(cmd1,))
    thread1.start()
    time.sleep(1)


    # чистим зациклинный фал от ошибок cc
    cmd2 = 'tsstamp ' + str(name) + '_firstfifo.ts ' + str(bitrate) + ' > ' + str(name) + '_secondfifo.ts'
    thread2 = threading.Thread(target=ossystem, args=(cmd2,))
    thread2.start()
    time.sleep(1)


    # стримим поток
    cmd3 = 'tsudpsend ' + str(name) + '_firstfifo.ts ' + str(ip) + ' ' + str(port) + ' ' + str(bitrate)
    thread3 = threading.Thread(target=ossystem, args=(cmd3,))
    thread3.start()
    time.sleep(1)


def run_streams():



    chanels = streams
    for chanel in chanels:
        if 'avaria' in chanel:
            temp = str(chanel['avaria']).split(':')
            file = 'avaria_' + chanel['name'] + '.ts'
            print temp
            thread1 = threading.Thread(target=stream, args=(file, temp[0], temp[1]))
            thread1.start()

        if 'interferencia' in chanel:
            temp = str(chanel['interferencia']).split(':')
            file = 'interferencia_' + chanel['name'] + '.ts'
            print temp
            thread1 = threading.Thread(target=stream, args=(file, temp[0], temp[1]))
            thread1.start()

        if 'avaria_mp4' in chanel:
            temp = str(chanel['avaria_mp4']).split(':')
            file = 'avaria_mp4_' + chanel['name'] + '.ts'
            print temp
            thread1 = threading.Thread(target=stream, args=(file, temp[0], temp[1]))
            thread1.start()

        if 'profilactika' in chanel:
            temp = str(chanel['profilactika']).split(':')
            file = 'profilactika_' + chanel['name'] + '.ts'
            print temp
            thread1 = threading.Thread(target=stream, args=(file, temp[0], temp[1]))
            thread1.start()

        if 'avaria_mp4_hd' in chanel:
            temp = str(chanel['avaria_mp4_hd']).split(':')
            file = 'moved_' + chanel['name'] + '.ts'
            print temp
            thread1 = threading.Thread(target=stream, args=(file, temp[0], temp[1]))
            thread1.start()

        if 'profilactika_mp4' in chanel:
            temp = str(chanel['profilactika_mp4']).split(':')
            file = 'profilactika_mp4_' + chanel['name'] + '.ts'
            print temp
            thread1 = threading.Thread(target=stream, args=(file, temp[0], temp[1]))
            thread1.start()
    # thread1.join()
    # thread1.join()
    # thread2.join()
    # thread3.join()
    # thread4.join()



run_streams()
