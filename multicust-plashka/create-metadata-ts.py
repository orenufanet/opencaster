#! /usr/bin/env python

#
# Copyright  2010, mediatvcom (http://www.mediatvcom.com/), Claude Vanderm. Based on Lorenzo Pallara scripts (l.pallara@avalpa.com) 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#                                  
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#                                  
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os
import time

from dvbobjects.PSI.PAT import *
from dvbobjects.PSI.NIT import *
from dvbobjects.PSI.SDT import *
from dvbobjects.PSI.PMT import *
from dvbobjects.DVB.Descriptors import *
from dvbobjects.MPEG.Descriptors import *
from dvbobjects.MHP.AIT import *
from dvbobjects.HBBTV.Descriptors import *
from chanels import streams

#
# Shared values
#


demo_transport_stream_id = 1  # demo value, an official value should be demanded to dvb org
demo_original_network_id = 1  # demo value, an official value should be demanded to dvb org

output_bitrate = '4000k'
start_pmt = 5000
step_ptm = 20
directory = os.path.dirname(os.path.abspath(__file__))

config = streams
for service_id_name in config:
    service_chanels = list()
    curent_pmt = start_pmt
    for chanel in service_id_name['chanels']:
        var = {
            'name': chanel['name'],
            'sid': chanel['sid'],
            'pmt': curent_pmt,
            'video_pid': 2064,
            # 'audio_pid': 2065,
        }
        service_chanels.append(var)
        curent_pmt += step_ptm

    #
    # Network Information Table
    # this is a basic NIT with the minimum desciptors, OpenCaster has a big library ready to use
    #

    dvb_service_descriptor = list()
    dvb_service_descriptor_mp4 = list()
    for stream in service_chanels:
        dvb_service_descriptor.append(service_descriptor_loop_item(
            service_ID=stream['sid'],
            service_type=1,  # digital tv service type
        ), )

        dvb_service_descriptor_mp4.append(service_descriptor_loop_item(
            service_ID=stream['sid'],
            service_type=0x16,  # digital tv service type
        ), )

    nit = network_information_section(
        network_id=1,
        network_descriptor_loop=[
            network_descriptor(network_name="Ufanet"),
        ],
        transport_stream_loop=[
            transport_stream_loop_item(
                transport_stream_id=demo_transport_stream_id,
                original_network_id=demo_original_network_id,
                transport_descriptor_loop=[
                    service_list_descriptor(  # Optional
                        dvb_service_descriptor_loop=dvb_service_descriptor,
                    ),
                ],
            ),
        ],
        version_number=3,
        # you need to change the table number every time you edit, so the decoder will compare its version with the new one and update the table
        section_number=0,
        last_section_number=0,
    )

    nit_mp4 = network_information_section(
        network_id=1,
        network_descriptor_loop=[
            network_descriptor(network_name="Ufanet"),
        ],
        transport_stream_loop=[
            transport_stream_loop_item(
                transport_stream_id=demo_transport_stream_id,
                original_network_id=demo_original_network_id,
                transport_descriptor_loop=[
                    service_list_descriptor(  # Optional
                        dvb_service_descriptor_loop=dvb_service_descriptor_mp4,
                    ),
                ],
            ),
        ],
        version_number=3,
        # you need to change the table number every time you edit, so the decoder will compare its version with the new one and update the table
        section_number=0,
        last_section_number=0,
    )
    #
    # Program Association Table (ISO/IEC 13818-1 2.4.4.3)
    #

    program = list()
    for stream in service_chanels:
        program.append(program_loop_item(
            program_number=stream['sid'],
            PID=stream['pmt'],
        ), )
    program.append(program_loop_item(
        program_number=0,  # special program for the NIT
        PID=16,
    ), )

    pat = program_association_section(
        transport_stream_id=demo_transport_stream_id,
        program_loop=program,
        version_number=1,
        # you need to change the table number every time you edit, so the decoder will compare its version with the new one and update the table
        section_number=0,
        last_section_number=0,
    )

    #
    # Program Map Table (ISO/IEC 13818-1 2.4.4.8)
    # this is PMT for HbbTV interactive applications
    #

    pmt = []
    pmt_mp4 = []

    for stream in service_chanels:
        pmt.append(program_map_section(
            program_number=stream['sid'],
            PCR_PID=stream['video_pid'],  # usualy the same than the video
            program_info_descriptor_loop=[],
            stream_loop=[
                stream_loop_item(
                    stream_type=2,  # mpeg2 video stream type
                    elementary_PID=stream['video_pid'],
                    element_info_descriptor_loop=[]
                ),
                # stream_loop_item(
                #     stream_type=3,  # mpeg2 audio stream type
                #     elementary_PID=stream['audio_pid'],
                #     element_info_descriptor_loop=[]
                # ),
            ],
            version_number=1,
            # you need to change the table number every time you edit, so the decoder will compare its version with the new one and update the table
            section_number=0,
            last_section_number=0,
        ))

        pmt_mp4.append(program_map_section(
            program_number=stream['sid'],
            PCR_PID=stream['video_pid'],  # usualy the same than the video
            program_info_descriptor_loop=[],
            stream_loop=[
                stream_loop_item(
                    stream_type=0x1b,  # mpeg4 video stream type
                    elementary_PID=stream['video_pid'],
                    element_info_descriptor_loop=[]
                ),
                # stream_loop_item(
                #     stream_type=28,  # mpeg4 audio stream type
                #     elementary_PID=stream['audio_pid'],
                #     element_info_descriptor_loop=[]
                # ),
            ],
            version_number=1,
            # you need to change the table number every time you edit, so the decoder will compare its version with the new one and update the table
            section_number=0,
            last_section_number=0,
        ))

    #
    # Service Description Table (ETSI EN 300 468 5.2.3)
    # this is a basic SDT with the minimum desciptors, OpenCaster has a big library ready to use
    #

    service = list()
    service_mp4 = list()
    for stream in service_chanels:
        service.append(service_loop_item(
            service_ID=stream['sid'],
            EIT_schedule_flag=0,  # 0 no current even information is broadcasted, 1 broadcasted
            EIT_present_following_flag=0,
            # 0 no next event information is broadcasted, 1 is broadcasted no current even information is broadcasted, 1 broadcasted
            running_status=4,  # 4 service is running, 1 not running, 2 starts in a few seconds, 3 pausing
            free_CA_mode=0,  # 0 means service is not scrambled, 1 means at least a stream is scrambled
            service_descriptor_loop=[
                service_descriptor(
                    service_type=1,  # digital television service
                    service_provider_name="AO Ufanet",
                    service_name=stream['name'],
                ),
            ],
        ))

        service_mp4.append(service_loop_item(
            service_ID=stream['sid'],
            EIT_schedule_flag=0,  # 0 no current even information is broadcasted, 1 broadcasted
            EIT_present_following_flag=0,
            # 0 no next event information is broadcasted, 1 is broadcasted no current even information is broadcasted, 1 broadcasted
            running_status=4,  # 4 service is running, 1 not running, 2 starts in a few seconds, 3 pausing
            free_CA_mode=0,  # 0 means service is not scrambled, 1 means at least a stream is scrambled
            service_descriptor_loop=[
                service_descriptor(
                    service_type=0x16,  # digital television service
                    service_provider_name="AO Ufanet",
                    service_name=stream['name'],
                ),
            ],
        ))

    sdt = service_description_section(
        transport_stream_id=demo_transport_stream_id,
        original_network_id=demo_original_network_id,
        service_loop=service,
        version_number=3,
        # you need to change the table number every time you edit, so the decoder will compare its version with the new one and update the table
        section_number=0,
        last_section_number=0,
    )
    sdt_mp4 = service_description_section(
        transport_stream_id=demo_transport_stream_id,
        original_network_id=demo_original_network_id,
        service_loop=service_mp4,
        version_number=3,
        # you need to change the table number every time you edit, so the decoder will compare its version with the new one and update the table
        section_number=0,
        last_section_number=0,
    )
    #
    # Application Informaton Table (ETSI TS 101 812 10.4.6)
    #
    #

    #
    # PSI marshalling and encapsulation
    #
    out = open("./nit.sec", "wb")
    out.write(nit.pack())
    out.close
    out = open("./nit.sec", "wb")  # python  flush bug
    out.close
    os.system('sec2ts 16 < ./nit.sec > ./nit.ts')

    out = open("./nit_mp4.sec", "wb")
    out.write(nit_mp4.pack())
    out.close
    out = open("./nit_mp4.sec", "wb")  # python  flush bug
    out.close
    os.system('sec2ts 16 < ./nit_mp4.sec > ./nit_mp4.ts')

    out = open("./sdt.sec", "wb")
    out.write(sdt.pack())
    out.close
    out = open("./sdt.sec", "wb")  # python   flush bug
    out.close
    os.system('sec2ts 17 < ./sdt.sec > ./sdt.ts')

    out = open("./sdt_mp4.sec", "wb")
    out.write(sdt_mp4.pack())
    out.close
    out = open("./sdt_mp4.sec", "wb")  # python   flush bug
    out.close
    os.system('sec2ts 17 < ./sdt_mp4.sec > ./sdt_mp4.ts')

    out = open("./pat.sec", "wb")
    out.write(pat.pack())
    out.close
    out = open("./pat.sec", "wb")  # python   flush bug
    out.close
    os.system('sec2ts 0 < ./pat.sec > ./pat.ts')

    i = 0

    pmt_tas = {}
    pmt_string = ''
    pmt_mp4_string = ''
    for stream in service_chanels:
        out = open("./pmt" + str(i) + ".sec", "wb")
        out.write(pmt[i].pack())
        out.close
        out = open("./pmt" + str(i) + ".sec", "wb")  # python   flush bug
        out.close
        os.system('sec2ts ' + str(stream['pmt']) + ' < ./pmt' + str(i) + '.sec > ./pmt' + str(i) + '.ts')
        pmt_string = pmt_string + 'b:3008 pmt' + str(i) + '.ts '

        out = open("./pmt_mp4" + str(i) + ".sec", "wb")
        out.write(pmt_mp4[i].pack())
        out.close
        out = open("./pmt_mp4" + str(i) + ".sec", "wb")  # python   flush bug
        out.close
        os.system('sec2ts ' + str(stream['pmt']) + ' < ./pmt_mp4' + str(i) + '.sec > ./pmt_mp4' + str(i) + '.ts')
        pmt_mp4_string = pmt_mp4_string + 'b:3008 pmt_mp4' + str(i) + '.ts '

        i += 1

    os.system('rm *.sec')  # deleting of the section files.

    # create config astra

    # avaria
    if 'avaria' in service_id_name:
        os.system(
            'tscbrmuxer c:2300000 tmp/video1.ts b:3008 pat.ts  b:1400 nit.ts b:1500 sdt.ts ' + pmt_string + '> output/avaria_' +
            service_id_name['name'] + '.ts')

    # interferencia
    if 'interferencia' in service_id_name:
        os.system(
            'tscbrmuxer c:2300000 tmp/video2.ts  b:3008 pat.ts  b:1400 nit.ts b:1500 sdt.ts ' + pmt_string + '> output/interferencia_' +
            service_id_name['name'] + '.ts')

    # avaria_mp4
    if 'avaria_mp4' in service_id_name:
        os.system(
            'tscbrmuxer c:2300000 tmp/video3.ts b:3008 pat.ts  b:1400 nit_mp4.ts b:1500 sdt_mp4.ts ' + pmt_mp4_string + '> output/avaria_mp4_' +
            service_id_name['name'] + '.ts')

    # profilactika
    if 'profilactika' in service_id_name:
        os.system(
            'tscbrmuxer c:2300000 tmp/video4.ts  b:3008 pat.ts  b:1400 nit.ts b:1500 sdt.ts ' + pmt_string + '> output/profilactika_' +
            service_id_name['name'] + '.ts')

    # avaria_mp4_hd
    if 'avaria_mp4_hd' in service_id_name:
        os.system(
            'tscbrmuxer c:2300000 tmp/video5.ts  b:3008 pat.ts  b:1400 nit.ts b:1500 sdt.ts ' + pmt_mp4_string + '> output/avaria_mp4_hd_' +
            service_id_name['name'] + '.ts')

    # profilactika_mp4
    if 'profilactika_mp4' in service_id_name:
        os.system(
            'tscbrmuxer c:2300000 tmp/video6.ts b:3008 pat.ts  b:1400 nit.ts b:1500 sdt.ts ' + pmt_mp4_string + '> output/profilactika_mp4_' +
            service_id_name['name'] + '.ts')

os.system('rm *.ts')
